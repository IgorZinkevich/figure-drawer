package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Trapezium extends Figure {
    private double radius;

    private Trapezium(int type, double cx, double cy, double lineWidth, Color color) {
        super(type, cx, cy, lineWidth, color);
    }

    public Trapezium(int type, double cx, double cy, double lineWidth, Color color, double radius) {
        this(type, cx, cy, lineWidth, color);
        this.radius = radius < 10 ? 10 : radius;
    }

     @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trapezium trapezium = (Trapezium) o;

        return Double.compare(trapezium.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(radius);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Trapezium{");
        sb.append("radius=").append(radius);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeLine(cx - radius * 2, cy + radius * 1.2, cx - radius, cy - radius * 1.2);
        gc.strokeLine(cx - radius , cy - radius * 1.2, cx + radius, cy - radius * 1.2);
        gc.strokeLine(cx + radius, cy - radius * 1.2, cx + radius * 2, cy + radius * 1.2);
        gc.strokeLine(cx + radius * 2, cy + radius * 1.2, cx - radius * 2, cy + radius * 1.2);
    }
}
