package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rhombus extends Figure {
    private double radius;

    private Rhombus(int type, double cx, double cy, double lineWidth, Color color) {
        super(type, cx, cy, lineWidth, color);
    }

    public Rhombus(int type, double cx, double cy, double lineWidth, Color color, double radius) {
        this(type, cx, cy, lineWidth, color);
        this.radius = radius < 10 ? 10 : radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rhombus rhombus = (Rhombus) o;

        return Double.compare(rhombus.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(radius);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rhombus{");
        sb.append("radius=").append(radius);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeLine(cx - radius * 1.2, cy , cx, cy - radius * 1.4);
        gc.strokeLine(cx , cy - radius * 1.4, cx + radius * 1.2, cy);
        gc.strokeLine(cx + radius * 1.2, cy, cx, cy + radius * 1.4);
        gc.strokeLine(cx, cy + radius * 1.4, cx - radius * 1.2, cy);
    }
}
