package com.stormnet.figuresfx.controller;

import com.stormnet.figuresfx.drawutils.Drawer;
import com.stormnet.figuresfx.figures.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController implements Initializable {
    private ArrayList<Figure> figures = new ArrayList<>();
    private Random random;

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(System.currentTimeMillis());
        System.out.println("Successfully initialized!");
    }

    private void addFigure(Figure figure) {
        figures.add(figure);
    }

    private Figure createFigure(double x, double y) {
        Figure figure = null;

        switch (random.nextInt(5)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(Figure.FIGURE_TYPE_CIRCLE, x, y, random.nextInt(3), Color.RED, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(Figure.FIGURE_TYPE_RECTANGLE, x, y, random.nextInt(3), Color.GREEN, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(Figure.FIGURE_TYPE_TRIANGLE, x, y, random.nextInt(3), Color.BLACK, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RHOMBUS:
                figure = new Rhombus(Figure.FIGURE_TYPE_RHOMBUS, x, y, random.nextInt(3), Color.BLUE, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_TRAPEZIUM:
                figure = new Trapezium(Figure.FIGURE_TYPE_TRAPEZIUM, x, y, random.nextInt(3), Color.GRAY, random.nextInt(50));
                break;
            default:
                System.out.println("Unknown figure!");
        }
        return figure;
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);
        drawer.draw(canvas.getGraphicsContext2D());
    }

    @FXML
    public void onMouseClicked(MouseEvent event) {
        addFigure(createFigure(event.getX(), event.getY()));
        repaint();
    }
}